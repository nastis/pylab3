from Person import Person

__author__ = 'mitroa'

try:
    people = [Person("Tom", 666666, "29/02/1996"), Person("Kathy", 123456, "31/12/2001")]

    with open('file.csv', 'w') as csvfile:
        csvfile.write(",".join(Person.csvorder))
        csvfile.write("\n")

        for element in people:
            mylist = []
            for order in Person.csvorder:
                mylist.append(str(getattr(element, order)))
            csvfile.write(",".join(mylist))
            csvfile.write("\n")

    with open('file.csv', 'r') as csvfile:
        people2 = []
        order = csvfile.readline().split(",")
        order_dict = {}
        for i in range(0, len(order)):
            order_dict[order[i].strip()] = i
        for line in csvfile:
            personstr = line.split(",")
            people2.append(Person(personstr[order_dict["_name"]], int(personstr[order_dict["_passport"]]), personstr[order_dict["_birthdate"]].strip()))

    for i in people2:
        print("Person" + ": " + i._name + ", " + str(i._passport) + ", " + i._birthdate)

except ValueError:
    print("Failed to add a person because of inappropriate parameters")
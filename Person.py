from datetime import datetime
from datetime import date

__author__ = 'mitroa'


def decorator_name(mylen):
    def wrap_one(set_name):
        def wrap_two(self, name):
            if 0 < len(name) <= mylen:
                set_name(self, name)
            else:
                raise ValueError("Name length must be less than or equal to " + str(mylen) + " symbols")
        return wrap_two

    return wrap_one


def decorator_passport():
    def wrap_one(set_passport):
        def wrap_two(self, passport):
            if 100000 <= passport <= 999999:
                set_passport(self, passport)
            else:
                raise ValueError("Passport number must be more than or equal to 100000 and less than or equal to 999999")

        return wrap_two

    return wrap_one


def decorator_birthdate():
    def wrap_one(set_birthdate):
        def wrap_two(self, birthdate):
            mydate = datetime.strptime(birthdate, "%d/%m/%Y").date()
            if date(1900, 1, 1) <= mydate < date.today():
                set_birthdate(self, birthdate)
            else:
                raise ValueError("Birth date must be between 1/1/1970 and current date")
        return wrap_two
    return wrap_one

# exceptions
# more general decorator for strings

class Person:
    def __init__(self, name, passport, birthdate):
        self.set_name(name)
        self.set_passport(passport)
        self.set_birthdate(birthdate)

    csvorder = ["_name", "_passport", "_birthdate"]

    @decorator_name(mylen=2)
    def set_name(self, name):
        self._name = name

    @decorator_passport()
    def set_passport(self, passport):
        self._passport = passport

    @decorator_birthdate()
    def set_birthdate(self, birthdate):
        self._birthdate = birthdate



